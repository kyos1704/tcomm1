#include<climits>
#include<algorithm>
#include<utility>
#include<set>
#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<cstdlib>
#include<ctime>
using namespace std;



///////////////////
// const set     //
///////////////////
bool DEBUG = 1;
typedef long long ll;
const int U = 0;
const int R = 1;
const int D = 2;
const int L = 3;
const string vec = "URDL";
const int vx[] = {-1,0,1,0};
const int vy[] = {0,1,0,-1};
const int playCount = 10000;
const int loop = playCount;

///////////////
// field data//
///////////////
int COLORS,X,Y;
clock_t start_t,end_t;

///////////////
// check pos //
///////////////
const bool checkPos(const int x,const int y){
  return (0<=x) && (x<X) && (0<=y) && (y<Y);
}

////////////////////
// struct replace //
////////////////////
class replaceField : public vector<vector<string>>{
  private:
    ll s;
    ll getNextBuf(){
      ll tmp = s;
      s = (tmp*48271LL) % 2147483647LL;
      return tmp;
    }
  public:
    replaceField():vector<vector<string>>(){};
    void setSeed(ll seed){
      s = seed;
      clear();
    };
    vector<string> operator[](int n){
      if(!(n<size()))for(int i=size();i<=n+5;i++){
        vector<string> tmp(2,string("00"));
        tmp[0][0] =char(getNextBuf()%COLORS)+'0';
        tmp[0][1] =char(getNextBuf()%COLORS)+'0';
        tmp[1][0] =char(getNextBuf()%COLORS)+'0';
        tmp[1][1] =char(getNextBuf()%COLORS)+'0';
        push_back(tmp); 
      }
      return vector<vector<string>>::at(n);
    }
    ll gets(){
      return s;
    }
};
replaceField RF;

//////////////////
// struct score //
//////////////////
struct score{
  int rand_num;
  int p;
  int near;
  score():p(0),near(0),rand_num(rand()%INT_MAX){};
  score(int x):p(x),near(0),rand_num(rand()%INT_MAX){};
  score(int x,int y):p(x),near(y),rand_num(rand()%INT_MAX){};
  bool get(){
    return p!=0;
  }
  bool isValid(){
    return p>=0;
  }
};
bool operator<(score a,score b){
  if(a.p!=b.p)return a.p<b.p;
  if(a.near!=b.near)return a.near<b.near;
  return a.rand_num<b.rand_num;
}
score operator+(score a,score b){
  return score(a.p+b.p);
}
/////////////////
// struct move //
/////////////////
typedef struct move{
  int x,y,v;
  move(const int xx,const int yy,const int vv):x(xx),y(yy),v(vv){
    check();
  }
  void check(){
    if((!checkPos(x+vx[v],y+vy[v]))||(!checkPos(x,y))){
      throw "undefind move ("+to_string(x)+","+to_string(y)+") "+vec[v];
    }
    if(!(0<=v&&v<4)){
      cerr<<"undefind vec";
      throw "undefind vec";
    }
  }
} M;
bool operator<(M a,M b){
  return a.x<b.x;
}
////////////////////////
// struct field       //
////////////////////////
class Field : public vector<string>{
private:
  int replace_all(){
    int res=0;
    for(int i=0;i<X;i++){
      for(int j=0;j<Y;j++){
        if(replace_judge(i,j)){
          replace(i,j);
          res++;
          i=max(0,i-3);
          j=max(-1,j-3);
        }
      }
    }
    return res;
  }
  int replace(int x,int y){
    vector<string> tmp = RF[rp_num];
    rp_num++;
    for(int i=0;i<tmp.size();i++){
      for(int j=0;j<tmp[i].size();j++){
        (*this)[x+i][y+j] = tmp[i][j];
      }
    }
  }
  void move(const M m){
    std::swap((*this)[m.x][m.y],(*this)[m.x+vx[m.v]][m.y+vy[m.v]]); 
    if(DEBUG)cerr<<"("<<m.x<<","<<m.y<<")"<<"<=>"<<"("<<m.x+vx[m.v]<<","<<m.y+vy[m.v]<<")"<<endl;
  }
public:
  int rp_num;
  Field():vector<string>(),rp_num(0){};
  Field(vector<string> s):vector<string>(s),rp_num(0){}
  Field& operator=(const vector<string>& s){
    vector<string>::operator=(s);
    return *this;
  }
  int init(){
    return replace_all();
  }
  void print(){
    if(!DEBUG)return ;
    for(int i=0;i<size();i++){
      cerr<<(*this)[i]<<endl;
    }cerr<<endl;
  }
  bool check(const int x,const int y){
    return checkPos(x,y);
  }
  typedef vector<pair<int,int>> matchList;
  const int match_cal(const int x,const int y,const matchList list){
    char c = (*this)[x][y];
    int res = 0;
    if(list.size()==0)return 0;
    for(int i=0;i<list.size();i++){
      if(checkPos(x+list[i].first,y+list[i].second)){
        if(c == (*this)[x+list[i].first][y+list[i].second]){
          res++;
        }
      }
    }
    return res;
  }
  const bool match(const int x,const int y,const matchList list){
    char c;
    if(list.size()==0)return false;
    if(checkPos(x+list[0].first,y+list[0].second)){
      c = (*this)[x+list[0].first][y+list[0].second];
    }else return false;
    for(int i=1;i<list.size();i++){
      if(!checkPos(x+list[i].first,y+list[i].second))return false;
      if(c != (*this)[x+list[i].first][y+list[i].second])return false;
    }
    return true;
  }
  const bool replace_judge(const int x,const int y){
    matchList ml = {
    {0,0},
    {1,0},
    {0,1},
    {1,1}
    };
    return match(x,y,ml);
  }
  int cal(const M m){
    const int min_x = min(m.x,m.x+vx[m.v]);
    const int max_x = max(m.x,m.x+vx[m.v]);
    const int min_y = min(m.y,m.y+vy[m.v]);
    const int max_y = max(m.y,m.y+vy[m.v]);
    int res = 0;
    for(int i=min_x-1;i<=max_x;i++){
      for(int j=min_y-1;j<=max_y;j++){
        if(replace_judge(i,j)){
          res++;
        }
      }
    }
    return res;
  }
  int cal_near(M tmp){
    const int x[] = {tmp.x,tmp.x+vx[tmp.v]};
    const int y[] = {tmp.y,tmp.y+vy[tmp.v]};
    int res = 0;
    matchList l = {
      {-1,-1},
      {-1, 0},
      {-1, 1},
      { 0,-1},
      { 0, 1},
      { 1,-1},
      { 1, 0},
      { 1, 1},
    };
    for(int i=0;i<2;i++){
      res += match_cal(x[i],y[i],l);
    }
    return res;
  }
  int move(const int x,const int y,const int v){
    try{
      move(M(x,y,v));
    }catch(string e){
      return -1;
    }
    return 1;
  }
  score tmp_move(const int x,const int y,const int v){
    try{
      const M tmp(x,y,v);
      if((*this)[x][y]==(*this)[x+vx[v]][y+vy[v]]){
        return -1;
      }
      move(tmp);
      const int res = cal(tmp);
      move(tmp);
      return score(res);
    }catch(string e){
      if(DEBUG)cerr<<"Err::"<<e<<endl;
      if(e=="undefind vec")cerr<<"Err"<<e<<endl;
      return score(-1);
    }
  }
  score tmp_near_move(const int x,const int y,const int v){
    try{
      const M tmp(x,y,v);
      if((*this)[x][y]==(*this)[x+vx[v]][y+vy[v]]){
        return -1;
      }
      const int pre_near = cal_near(tmp);
      move(tmp);
      const int aft_near = cal_near(tmp);
      const int res = cal(tmp);
      move(tmp);
      return score(res,aft_near-pre_near);
    }catch(string e){
      if(DEBUG)cerr<<"Err::"<<e<<endl;
      if(e=="undefind vec")cerr<<"Err"<<e<<endl;
      return score(-1);
    }
  }
  int def_move(const int x,const int y,const int v){
    try{
      const M tmp(x,y,v);
      move(tmp);
      return replace_all();
    }catch(string e){
      if(DEBUG)cerr<<"Err::"<<e<<endl;
      return -1;
    }
  }
};
typedef Field F;

///////////
// solve //
///////////
int patt[10]={};
vector<M> quick_gen(F f){
  typedef pair<score,vector<M>> pSVM;
  vector<pSVM> tmp;
  for(int i=0;i<X;i++){
    for(int j=0;j<Y;j++){
      for(int k=0;k<2;k++){
        score c = f.tmp_near_move(i,j,k);
        if(!c.isValid())continue;
        tmp.push_back(pSVM(c, vector<M>(1,M(i,j,k))) );
      }
    }
  }
  if(tmp.size()==0){
    return vector<M>();
  }
  sort(tmp.rbegin(),tmp.rend());
  if(tmp[0].first.get())patt[tmp[0].second.size()]++;
  return tmp[0].second;
}

vector<M> gen(F f){
  typedef pair<score,vector<M>> pSVM;
  vector<pSVM> tmp;
  for(int i=0;i<X;i++){
    for(int j=0;j<Y;j++){
      for(int k=0;k<2;k++){
        score c = f.tmp_near_move(i,j,k);
        if(!c.isValid())continue;
        if(c.get()){
          tmp.push_back(pSVM(c, vector<M>(1,M(i,j,k))) );
        }else{
          tmp.push_back(pSVM(c, vector<M>(1,M(i,j,k))) );
          try{
            f.move(i,j,k);
            const int x[2] = {i,i+vx[k]};
            const int y[2] = {j,j+vy[k]};
            for(int kk=0;kk<4;kk++){
              for(int l=0;l<2;l++){
                if(k!=(kk+l*2)%4){
                  try{
                    score cc = f.tmp_move(x[l],y[l],kk);
                    if(!cc.get())continue;
                    vector<M> ttmp;
                    ttmp.push_back(M(i,j,k));
                    ttmp.push_back(M(x[l],y[l],kk));
                    if(cc.get()){
                      tmp.push_back(pSVM(c+cc ,ttmp ));
                    }
                  }catch(...){
                    continue;
                  }
                }
              }
            }
            f.move(i,j,k);
          }catch(...){
            continue;
          }
        }
      }
    }
  }
  if(tmp.size()==0){
    return vector<M>();
  }
  sort(tmp.rbegin(),tmp.rend());
  if(tmp[0].first.get())patt[tmp[0].second.size()]++;
  return tmp[0].second;
}

vector<M> solve(F f){
  int c = f.init();
  cerr<<"solve::"<<c<<endl;
  vector<M> res;
  while(res.size()<10000&&clock()<end_t){
    vector<M> tmp;
    if(clock() > end_t - 3.5 * CLOCKS_PER_SEC){
      tmp = quick_gen(f);
      if(res.size()%1000==0)cerr<<"point::"<<c<<"  res:"<<res.size() << " quick"<<endl;
    }else{
      tmp = gen(f);
      if(res.size()%1000==0)cerr<<"point::"<<c<<"  res:"<<res.size()<<endl;
    }
    if(tmp.size()==0){
      return res;
    }
    for(auto i:tmp){
      int tmp = f.def_move(i.x,i.y,i.v);
      if(tmp==-1){
        cerr<<"err move::("<<i.x<<","<<i.y<<"):"<<vec[i.v]<<endl;
        break;
      }
      c += tmp;
      res.push_back(i);
    }
    f.print();
  }
  cerr<<"board=("<<X<<","<<Y<<"):"<<COLORS<<endl;
  cerr<<"max score ::" << c<<" get Score::"<<patt[2]+patt[1]<<endl;
  for(int i=0;i<10;i++){
    cerr<<"patt["<<i<<"]::"<<patt[i]<<endl;
  }
  return res;
}

//////////////////////////////////////////////////////
//     main                                         */
//////////////////////////////////////////////////////

vector<int> ans;
void ans_set(int n,int x,int y,int v){
  if(ans.size()!=30000){
    ans.resize(30000);
  }
  if(n>=10000)return ;
  if(ans.size()!=playCount*3)ans.resize(playCount*3);
  n*=3;
  ans[n]=x;
  ans[n+1]=y;
  ans[n+2]=v;
}
class SquareRemover{
  public:
    void init(const int c,const vector<string> field,const ll startSeed){
      X = field.size();
      Y = field[0].size();
      COLORS = c;
      RF.setSeed(startSeed);
    }
    vector<int> playIt(int c,vector<string> field,ll startSeed){
      cerr<<"playIt"<<endl;
      init(c,field,startSeed);
      start_t = clock();
      end_t = start_t + 28 * CLOCKS_PER_SEC;
      srand(time(NULL));
      //if(DEBUG)return ans;
      DEBUG=0;
      vector<M> a;
      try{
        a = solve(field);
      }catch(string e){
        cerr<<e<<endl;
      }
      for(int i=0;i<10000;i++){
        if(i<a.size()){
          ans_set(i,a[i].x,a[i].y,a[i].v);
        }else{
          ans_set(i,rand()%(field.size()-2)+1,rand()%(field[0].size()-2)+1,rand()%4);
        }
      }
      cerr<<"ans.size::"<<a.size()<<endl;
      return ans;
    }
};
//BEGINCUT
int main(){
  cerr<<"start"<<endl;
  ll nn,cc;
  cin>>cc>>nn;
  vector<string> input;
  for(int i=0;i<nn;i++){
    string tmp;
    cin>>tmp;
    input.push_back(tmp);
  }
  ll ss;
  cin>>ss;
  cerr<<"fin input"<<endl;
  SquareRemover test;
  vector<int> a = test.playIt(cc,input,ss);
  for(int i=0;i<a.size();i++){
    cout<<a[i]<<endl;
  }
  cerr<<"("<<X<<","<<Y<<")"<<":"<<cc<<endl;
  cerr<<"time::"<< (long double)(clock() - start_t)/CLOCKS_PER_SEC<<" sec"<<"  rest::"<<((long double)(end_t - clock())/CLOCKS_PER_SEC)<<" per::"<<(long double)(clock()-start_t)/(29*CLOCKS_PER_SEC)<<endl;
  fflush(stdout);
}
//CUTEND
