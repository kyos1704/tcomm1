#include<algorithm>
#include<cstdlib>
#include<vector>
#include<cstdio>
#include<iostream>
using namespace std;
int X,Y;
typedef long long ll;
typedef vector<string> F;
const int U = 0;
const int R = 1;
const int D = 2;
const int L = 3;
const string vec = "URDL";
const int vx[] = {-1,0,1,0};
const int vy[] = {0,1,0,-1};
clock_t start_t,end_t;
const int playCount = 10000;
const int loop = playCount;
const int DEBUG = false;

ll getNextBuf(ll s){
  return (s*48271LL) % 2147483647LL;
}

struct M{
  int x,y,v,p;
  bool check(){
    if((0<=x+vx[v])
        &&(0<=y+vy[v])
        &&(x+vx[v]<X)
        &&(y+vy[v]<Y)){
      return true;
    }
    return false;
  }
};
bool operator<(M a,M b){
  return a.p>b.p;
}
struct S{
  int c;
  F f;
  int seed;
  int n;
  int p;
  S(int C,F field,int S,int N){
    n=N;
    c = C;
    f = field;
    seed=S;
    p=0;
  }
  void printall(){
    if(!DEBUG)return ;
    cerr<<p<<" "<<seed<<endl;
    for(int i=0;i<f.size();i++){
      cerr<<f[i]<<endl;
    }cerr<<endl;
    cerr<<seed%c<<endl;
    cerr<<endl;
  }
  ll getbuf(){
    int tmp = seed;
    seed = getNextBuf(seed);
    return tmp;
  }
  void replace(int x,int y){
    //cerr<<"replace::("<<x<<","<<y<<")"<<endl;
    f[x][y]=char('0'+(getbuf()%c));
    f[x][y+1]=char('0'+(getbuf()%c));
    f[x+1][y]=char('0'+(getbuf()%c));
    f[x+1][y+1]=char('0'+(getbuf()%c));
  }
  bool flush_f(){
    for(int i=0;i<f.size()-1;i++){
      for(int j=0;j<f[0].size()-1;j++){
        if(f[i][j]==f[i+1][j]
          &&f[i][j]==f[i][j+1]
          &&f[i][j]==f[i+1][j+1]){
          replace(i,j);
          p++;
          return true;
        }
      }
    }
    return false;
  }
  void move(M nex){ 
    const int x = nex.x;
    const int y = nex.y;
    const int v = nex.v;
    swap(f[x][y],f[x+vx[v]][y+vy[v]]);
    while(flush_f());
  }
  S getNext(M nex){
    if(nex.check()==false)return S(0,F(),0,-1);
    S res(c,f,seed,n);
    res.move(nex);
    return res;
  }
};
struct MS{
  M m;
  S s;
};
bool operator<(MS a,MS b){
  return a.m<b.m;
}
MS nextGen(S s){
  vector<MS> ans;
  for(int i=0;i<X;i++){
    for(int j=0;j<Y;j++){
      for(int k=0;k<2;k++){
        M m;
        m.x = i;
        m.y = j;
        m.v = k;
        if(!m.check())continue;
        S tmp = s.getNext(m);
        m.p = tmp.p;
        if(m.p!=-1&&tmp.f!=s.f){
          ans.push_back((MS){m,tmp});
        }
        }
      }
    }
  sort(ans.begin(),ans.end());
  if(ans[0].m.p>0){
    return ans[0];
  }else{
    int t = rand()%ans.size();
    return ans[t];
  }
}

vector<M> solve(S s){
  vector<M> res;
  s.printall();
  int count = 0;
  while(res.size()<loop&& clock()-start_t < 28 * CLOCKS_PER_SEC){
    if(DEBUG&&res.size()%100==0)cerr<<res.size()<<endl;
    MS next = nextGen(s);
    s = next.s;
    res.push_back(next.m);
  }
  if(DEBUG)cerr<<count<<endl;
  return res;
}

vector<int> ans;
void set(int n,int x,int y,int v){
  if(n>=playCount)return ;
  if(ans.size()!=playCount*3)ans.resize(playCount*3);
  n*=3;
  ans[n]=x;
  ans[n+1]=y;
  ans[n+2]=v;
}
class SquareRemover{
  public:
    vector<int> playIt(int colors,vector<string> field,int startSeed){
      X = field.size();
      Y = field[0].size();
      start_t = clock();
      srand(time(NULL));
      for(int i=0;i<playCount;i++){
        set(i,rand()%(field.size()-2)+1,rand()%(field[0].size()-2)+1,rand()%4);
      }
      S s(colors,field,startSeed,0);
      vector<M> a = solve(s);
      for(int i=0;i<a.size();i++){
        set(i,a[i].x,a[i].y,a[i].v);
        //cerr<< i<<":("<<a[i].x<<","<<a[i].y<<")"<<vec[a[i].v]<<endl;
      }
      cerr<<"ans.size::"<<a.size()<<endl;
      end_t = clock();
      return ans;
    }
};
//BEGINCUT
int main(){
  int nn,cc;
  cin>>cc>>nn;
  vector<string> input;
  for(int i=0;i<nn;i++){
    string tmp;
    cin>>tmp;
    input.push_back(tmp);
  }
  int ss;
  cin>>ss;
  SquareRemover test;
  vector<int> a = test.playIt(cc,input,ss);
  for(int i=0;i<a.size();i++){
    cout<<a[i]<<endl;
  }
  cerr<<"("<<X<<","<<Y<<")"<<":"<<cc<<endl;
  cerr<<"time::"<< (long double)(end_t - start_t)/CLOCKS_PER_SEC<<" sec"<<endl;
  fflush(stdout);
}
//CUTEND
