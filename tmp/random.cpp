#include<algorithm>
#include<cstdlib>
#include<vector>
#include<cstdio>
#include<iostream>
using namespace std;

typedef long long ll;

const int U = 0;
const int R = 1;
const int D = 2;
const int L = 3;
const string vec = "URDL";
const int vx[] = {-1,0,1,0};
const int vy[] = {0,1,0,-1};
const int playCount = 10000;
vector<int> ans;
void set(int n,int x,int y,int v){
  if(n>=playCount){
    cerr<<"????????????????"<<endl;
    return ;
  }
  ans[3*n]   = x;
  ans[3*n+1] = y;
  ans[3*n+2] = v;
}
long long int c,s;
vector<string> f;
void printall(){
  for(int i=0;i<f.size();i++){
    //cerr<<f[i]<<endl;
  }//cerr<<endl;
}
ll getbuf(){
  ll res = s;
  s=(s*48271LL) % 2147483647LL;
  return res;
}
void replace(int x,int y){
  f[x][y]=char('0'+(getbuf()%c));
  f[x][y+1]=char('0'+(getbuf()%c));
  f[x+1][y]=char('0'+(getbuf()%c));
  f[x+1][y+1]=char('0'+(getbuf()%c));
}
bool flush_f(){
  for(int i=0;i<f.size()-1;i++){
    for(int j=0;j<f[0].size()-1;j++){
      if(f[i][j]==f[i+1][j]&&f[i][j]==f[i][j+1]&&f[i][j]==f[i+1][j+1]){
        replace(i,j);
        return true;
      }
    }
  }
  return false;
}
void convert(int n,int x,int y,int v){
  set(n,x,y,v);
  swap(f[x][y],f[x+vx[v]][y+vy[v]]);
  while(flush_f());
}
vector<pair<int,int> > a;
int mode=0;
int nextSet(int n){
  for(int k=0;k<a.size();k++){
    int max_c = k;
    bool rev= false;
    if(mode/2==1){
      max_c = a.size()-k-1;
      rev=true;
    }
    for(int i=f.size()-1;i>=0;i--){
      for(int j=f[0].size()-1;j>=0;j--){
        if(f[i][j]==char('0'+max_c)){
          if(0<=i+vx[mode])
          if(0<=j+vy[mode])
          if(i+vx[mode]<f.size())
          if(j+vy[mode]<f[0].size())
          if((!rev&&f[i+vx[mode]][j+vy[mode]]>'0'+max_c)
            ||(rev&&f[i+vx[mode]][j+vy[mode]]<'0'+max_c)){
            convert(n,i,j,mode);
            return n+1;
          }
        }
      }
    }
  }
  return n;
}

class SquareRemover{
  public:
    vector<int> playIt(int colors,vector<string> field,int startSeed){
      clock_t start = clock(),end;
      c = colors;f = field;s = startSeed;
      srand(time(NULL));
      ans = vector<int>(playCount*3,1);
      for(int i=0;i<playCount;i++){
        set(i,rand()%(f.size()-2)+1,rand()%(f[0].size()-2)+1,rand()%4);
      }
      int n=0;
      a.resize(c);
      /*while(n<playCount&&(clock()-start)/CLOCKS_PER_SEC<10){
        if(n==0||n%500==0){
          a.clear();
          a.resize(c);
          for(int i=0;i<a.size();i++){
            a[i].first=rand()%100;
          }
          for(int i=0;i<a.size();i++){
            a[i].second=i;
          }
          sort(a.begin(),a.end());
        }
        int tmp=nextSet(n);
        if(tmp==n||n%2000==0){
          mode = (mode+1)%4;
        }
        n=tmp;
        if(n%1000==0)cerr<<n<<endl;
      }*/
      end = clock();
      cerr << (end-start)/(double)CLOCKS_PER_SEC <<"sec"<<endl;
      return ans;
    }
};


int main(){
  int nn;
  int cc;
  cin>>cc>>nn;
  vector<string> input;
  for(int i=0;i<nn;i++){
    string tmp;
    cin>>tmp;
    input.push_back(tmp);
  }
  int ss;
  cin>>ss;
  SquareRemover test;
  vector<int> a = test.playIt(cc,input,ss);
  for(int i=0;i<a.size();i++){
    cout<<a[i]<<endl;
  }
  fflush(stdout);
}
